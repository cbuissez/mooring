<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Services
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @author      Charly Piva
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$services = [
    
    'Gérer les établissements' => [
        'shortname'             => 'school',
        'functions'             => ['local_mooring_get_schools'],
        'requiredcapability'    => 'local/mooring:services',               
        'restrictedusers'       => 0,
        'enabled'               => 1
    ],
    'Obtenir les statistiques' => [
        'shortname'             => 'stats',
        'functions'             => ['local_mooring_stats','local_mooring_lastlogs'],
        'requiredcapability'    => 'local/mooring:services',               
        'restrictedusers'       => 0,
        'enabled'               => 1
    ],
    'Lire et modifier les paramètres des plugins' => [
        'shortname'             => 'pluginparam',
        'functions'             => ['local_mooring_get_plugin_param','local_mooring_set_plugin_param','local_mooring_get_param_list'],
        'requiredcapability'    => 'local/mooring:services',
        'restrictedusers'       => 0,
        'enabled'               => 1
    ]
    
];

$functions = [
    
    /*'local_mooring_create_schools' => [
        'classname'     => 'local_mooring_external',
        'methodname'    => 'create_schools',
        'classpath'     => 'local/mooring/externallib.php',
        'description'   => 'Creates new schools',
        'type'          => 'write',
        //'services'      => []
    ],*/
    'local_mooring_set_plugin_param' => [
        'classname'     => 'local_mooring_external',
        'methodname'    => 'set_plugin_param',
        'classpath'     => 'local/mooring/externallib.php',
        'description'   => 'Set plugin param',
        'type'          => 'write',
        //'services'      => []
    ],
    'local_mooring_get_plugin_param' => [
        'classname'     => 'local_mooring_external',
        'methodname'    => 'get_plugin_param',
        'classpath'     => 'local/mooring/externallib.php',
        'description'   => 'Get plugin param',
        'type'          => 'read',
        //'services'      => []
    ],
    'local_mooring_get_param_list' => [
        'classname'     => 'local_mooring_external',
        'methodname'    => 'get_param_list',
        'classpath'     => 'local/mooring/externallib.php',
        'description'   => 'Get plugin parameters list',
        'type'          => 'read',
        //'services'      => []
    ],
    'local_mooring_get_schools' => [
        'classname'     => 'local_mooring_external',
        'methodname'    => 'get_schools',
        'classpath'     => 'local/mooring/externallib.php',
        'description'   => 'Get all existing schools',
        'type'          => 'read',
        //'services'      => []
    ],
    'local_mooring_stats' => [
        'classname'     => 'local_mooring_external',
        'methodname'    => 'stats',
        'classpath'     => 'local/mooring/externallib.php',
        'description'   => 'Get stats from all schools',
        'type'          => 'read',
        //'services'      => []
    ],
    'local_mooring_lastlogs' => [
        'classname'     => 'local_mooring_external',
        'methodname'    => 'lastlogs',
        'classpath'     => 'local/mooring/externallib.php',
        'description'   => 'Get last logs from all schools',
        'type'          => 'read',
        //'services'      => []
    ]
    
];