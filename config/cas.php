<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * CAS configuration
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

return [
    
    "elancourt" => [
        "name"      => "Elancourt",
        "import"    => "itop",
        "criteria"  => [
            "uai"       => ["0781968r", "0782124k", "0783514w"]
        ]
    ],
    
    "entidf"    => [
        "name"      => "monlycée.net",
        "import"    => "entcorespecial",
        "criteria"  => [
            "nature"    => ["lycee"]
        ]
    ],

    "oze78"     => [
        "name"      => "eCollège (OZE)",
        "import"    => "oze",
        "criteria"  => [
            "nature"    => ["college"],
            "dept"      => [78]
        ]
    ],

    "oze781d"     => [
        "name"      => "Yvelines Numériques 1D (OZE)",
        "import"    => "oze",
        "criteria"  => [
            "nature"    => ["ecole"],
            "dept"      => [78],
            "uai"       => ["0780306j", "0780791l", "0780824x", "0780876d", "0782122h"]
        ]
    ],

    "ent91"     => [
        "name"      => "MonCollege.essonne.fr",
        "import"    => "entcore",
        "criteria"  => [
            "nature"    => ["college"],
            "dept"      => [91]
        ]
    ],

    "oze92"     => [
        "name"      => "ENC 92 (OZE)",
        "import"    => "oze",
        "criteria"  => [
            "nature"    => ["college"],
            "dept"      => [92]
        ]
    ],

    "kos95"     => [
        "name"      => "MonCollege 95",
        "import"    => "kosmos",
        "criteria"  => [
            "nature"    => ["college"],
            "dept"      => [95]
        ]
    ]
    
];
