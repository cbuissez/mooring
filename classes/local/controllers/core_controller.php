<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Core controller
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_mooring\local\controllers;

class core_controller {
    
    protected $viewpath;
    protected $template;
    
    public function ajax($func) {
        try {
            $response = new \stdClass();
            $response->data = $this->$func();
        } catch (\Exception $e) {
            $response->error = $e->getMessage();
        } finally {
            echo json_encode($response);
        }
    }
    
    protected function render($view, $variables = []) {
        extract($variables);
        ob_start();
        require($this->viewpath . str_replace('.', '/', $view) . '.php');
        $classname = substr(strrchr(get_class($this), '\\'), 1);
        if (strpos($classname, 'page')) {
            $content = ob_get_clean();
            require($this->viewpath . 'templates/' . $this->template . '.php');
        }
    }
    
    protected function forbidden() {
        header('HTTP/1.0 403 Forbidden');
        die('Accès interdit');
    }
    
    public function not_found() {
        header('HTTP/1.0 404 Not Found');
        die('Page introuvable');
    }
    
}
