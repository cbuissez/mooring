<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Application slack model
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_mooring\local\models;

class app_slack extends core_slack {
    
    public $available = false;
    
    public function __construct() {
        global $USER;
        $this->url = get_config('local_mooring', 'slackurl');
        $this->channel = get_config('local_mooring', 'slackchannel');
        $this->username = $_SERVER['SERVER_NAME'];
        $this->text = "[$USER->firstname $USER->lastname ($USER->username)](mailto:$USER->email)";
        if (isset($this->url, $this->channel)) {
            $this->available = true;
        }
    }
    
    /*public function delete_last_attachment() {
        array_pop($this->attachments);
        return $this;
    }*/
    
    public function array_to_fields($array) {
        foreach ($array as $key => $value) {
            $this->field(['title' => $key, 'value' => (string) $value]);
        }
        return $this;
    }
    
    public function transmit_exception($e) {
        if ($this->available) {
            $this->field([
                'title' =>  "Message",
                'value' =>  (string) $e->getMessage()
            ])->field([
                'title' =>  "Fichier",
                'value' =>  (string) substr($e->getFile(), strlen($_SERVER['DOCUMENT_ROOT']))
            ])->field([
                'title' =>  "Ligne",
                'value' =>  (string) $e->getLine()
            ])->attachment([
                'color' =>  'danger',
                'title' =>  "L'exception suivante s'est produite :"
            ])->array_to_fields($_GET)->attachment([
                'color' =>  'danger',
                'title' =>  "Données GET de la requête :"
            ])->array_to_fields($_POST)->attachment([
                'color' =>  'danger',
                'title' =>  "Données POST de la requête :"
            ])->send();
        }
    }
    
}

