<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Application user model
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_mooring\local\models;

use local_mooring\local\config;

class app_user extends core_user {
    
    protected $user;
    
    public function get_username() {
        if (isset($this->user->username)){
            return $this->user->username;
        }
        return null;
    }
    
    public function get_userid() {
        if (isset($this->user->id)){
            return $this->user->id;
        }
        return null;
    }
    
    public function __construct($data, $cas) {
        $this->user = $data;
        
        if (!$this->user->firstname) {
            throw new \Exception("APP USER: missing firstname");
        }
        if (!$this->user->lastname) {
            throw new \Exception("APP USER: missing lastname");
        }
        
        if (!$this->user->profil) {
            throw new \Exception("APP USER: missing profil");
        }
        if (!$this->user->uai) {
            throw new \Exception("APP USER: missing uai");
        }
        
        if ($cas) {
            if (!$this->user->username) {
                throw new \Exception("APP USER : missing username");
            }
            $this->user->username = $cas . '.' . $this->user->username;
            $this->user->auth = 'eleacas';
        }  
    }
    
    public function update_or_create($updtpasswd = false) {
        if ($this->is_exist()) {
            if ($this->authorized_update()) {
                if (!$updtpasswd) {
                    unset($this->user->hash);
                }
                $this->update();
                return 'updated';
            } else {
                return 'known';
            }
        } else {
            $this->create();
            return 'created';
        }
    }
    
    public function create($useless = null) {
        $this->user->id = parent::create($this->prepare_moodle_user());
        $this->set_fields($this->user->id, $this->prepare_extra_fields());
    }
    
    private function prepare_moodle_user() {
        $moodle = (object) [
            'firstname' => $this->user->firstname,
            'lastname'  => $this->user->lastname,
        ];
        if (isset($this->user->username)) {
            $moodle->username = $this->user->username;
        } else {
            $moodle->username = $this->create_username();
        }
        if (isset($this->user->auth)) {
            $moodle->password = 'password';
            $moodle->auth = $this->user->auth;
        }
        if (isset($this->user->hash)) {
            $moodle->password = $this->user->hash;
        }
        if (isset($this->user->email)) {
            $moodle->email = $this->user->email;
        } else {
            $moodle->email = config::load('base')->get('defaultmail') . '.invalid';
        }
        return $moodle;
    }
    
    private function prepare_extra_fields() {
        $extra = (object) [
            'profil'    => $this->user->profil,
            'uai'       => $this->user->uai
        ];
        if ($this->user->profil === 'teacher') {
            $extra->rne1 = $this->user->uai;
        }
        if (isset($this->user->siecle)) {
            $extra->siecle = $this->user->siecle;
        }
        if (isset($this->user->special)) {
            $extra->disambiguation = $this->user->special;
        }
        if (isset($this->user->timestamp)) {
            $extra->timeretrieved = $this->user->timestamp;
        } else {
            $extra->timeretrieved = time();
        }
        return $extra;
    }
    
    public function update($useless = null) {
        global $DB;
        if (empty($this->user->auth)) {
            $olduser = $DB->get_record('user', ['id' => $this->user->id], 'auth, username', MUST_EXIST);
            if ($olduser->auth === 'manual') {
                $this->user->username = $olduser->username;
            }
        }
        $user = $this->prepare_moodle_user();
        unset($user->email);
        $user->id = $this->user->id;
        parent::update($user);
        $this->set_fields($this->user->id, $this->prepare_extra_fields());
    }
    
    private function is_exist() {
        if ($this->user->username && $this->is_exist_username()) {
            return true;
        }
        if ($this->user->siecle && $this->is_exist_siecle()) {
            return true;
        }
        if ($this->user->special && $this->is_exist_special()) {
            return true;
        }
        return false;
    }
    
    private function is_exist_username() {
        global $DB;
        $record = $DB->get_record('user', ['username' => $this->user->username], 'id');
        if (!$record) {
            return false;
        } else {
            $this->user->id = $record->id;
            return true;
        }
    }
    
    private function is_exist_siecle() {
        global $DB;
        $records = $DB->get_records('user_info_data', ['fieldid' => $this->get_field_id('siecle')], '', 'userid, data');
        foreach ($records as $record) {
            if ($record->data === $this->user->siecle) {
                $this->user->id = $record->userid;
                return true;
            }
        }
        return false;
    }
    
    private function is_exist_special() {
        global $DB;
        $records = $DB->get_records('user', [
            'firstname' => $this->user->firstname,
            'lastname'  => $this->user->lastname
        ], '', 'id');
        foreach ($records as $record) {
            $disambiguation = $DB->get_record('user_info_data', [
                'userid'    => $record->id,
                'fieldid'   => $this->get_field_id('disambiguation')
            ], 'data');
            if (is_object($disambiguation) && $disambiguation->data == $this->user->special) {
                $uai = $DB->get_record('user_info_data', [
                    'userid'    => $record->id,
                    'fieldid'   => $this->get_field_id('uai')
                ], 'data');
                if (is_object($uai) && $uai->data == $this->user->uai) {
                    $this->user->id = $record->id;
                    return true;
                }
            }
        }
        return false;
    }
    
    private function authorized_update() {
        global $DB;
        $record = $DB->get_record('user_info_data', [
            'fieldid'   => $this->get_field_id('timeretrieved'),
            'userid'    => $this->user->id
        ], 'data');
        if ($record) {
            $timeretrieved = (int) $record->data;
        } else {
            $timeretrieved = 0;
        }
        if ($this->user->timestamp > $timeretrieved) {
            return true;
        } else {
            return false;
        }
    }
    
    private function create_username() {
        global $DB;
        $username = $this->username_clean_part($this->user->firstname);
        $username .= '.';
        $username .= $this->username_clean_part($this->user->lastname);
        while ($DB->get_record('user', ['username' => $username], 'username')) {
            $matches = [];
            preg_match('#^([^0-9]+)([0-9]*)$#', $username, $matches);
            if($matches[0] == $matches[1]) {
                $username = $matches[0].'1';
            } else {
                $username = $matches[1].($matches[2] + 1);
            }
        }
        $this->user->username = $username;
        return $username;
    }
    
    private function username_clean_part($text) {
        // Ne conserve que les caractères alphabétiques (on cherche à retirer les espaces et les tirets)
        $part = preg_replace('#[^a-z]#', '',
                // Remplace les caractères accentués par leur équivalant ASCII le plus proche
                // ATTENTION Bien vérfier que la translittération fonctionne sinon ces caractères seront supprimés !
                iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE',
                        strtolower($text)));
        // On limite le nombre de caractères pour chaque élément de l'identifiant
        return substr($part, 0, 10);
    }

}
