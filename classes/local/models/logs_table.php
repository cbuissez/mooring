<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Logs table model
 *
 * @package     local_mooring
 * @author      Charly Piva
 * @copyright   (C) Charly Piva 2017
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_mooring\local\models;

class logs_table {
    
    private $user_info_field_profil;
    private $user_info_field_uai;
    
    public function __construct($profil, $uai){
        $this->user_info_field_profil = $profil;
        $this->user_info_field_uai = $uai;
    }
    
    //Compte le nombre de connexions uniques, par défaut toutes les 30 minutes 
    public function get_user_counts($time = false, $interval = 1800, $steps = 50){
        global $DB;
        if(!$time) $time = time();
        $time = floor($time/$interval)*$interval;
        $timestamp_start = $time;
        $user_counts = [];
        for($i = 0; $i < $steps; $i++){
            $sql = 'SELECT COUNT(DISTINCT userid) FROM {logstore_standard_log} '
                    . 'WHERE timecreated >= :timestart AND timecreated < :timeend '
                    . 'AND target NOT LIKE "webservice_%" AND userid > 2';
            $user_counts[] = $DB->count_records_sql($sql,[
                'timestart' => $time-$interval,
                'timeend'   => $time,
            ]); 
            $time -= $interval;
        }
        return (object) [
            'values'            => array_reverse($user_counts),
            'timestamp_start'   => $timestamp_start,
            'interval'          => $interval
        ];
    }
    
    //Compte le nombre de connexions pour la journée en cours
    public function get_current_day_connections($time = false){
        global $DB;
        if(!$time) $time = time();
        $time = floor($time/86400)*86400;
        
        //Total d'utilisateurs connextés
        $sql = 'SELECT COUNT(DISTINCT userid) FROM {logstore_standard_log} '
                . 'WHERE timecreated >= :timestart '
                . 'AND target NOT LIKE "webservice_%" AND userid > 2';
        $connections = $DB->count_records_sql($sql,[
            'timestart' => $time,
        ]);
        
        //Total de tentatives de connexions
        $sql = 'SELECT COUNT(*) FROM {logstore_standard_log} '
                . 'WHERE timecreated >= :timestart '
                . 'AND action = "loggedin" AND userid > 2';
        $logins = $DB->count_records_sql($sql,[
            'timestart' => $time,
        ]);
        
        //Connexions d'élèves et d'enseignants
        $sql = 'SELECT COUNT(DISTINCT s.userid) '
                . 'FROM {logstore_standard_log} s INNER JOIN {user_info_data} d ON d.userid = s.userid '
                . 'WHERE d.fieldid = :field_profil AND d.data = :profil AND timecreated >= :timestart AND s.userid > 2';
        $students = $DB->count_records_sql($sql, [
            'profil'    => 'student',
            'timestart' => $time,
            'field_profil' => $this->user_info_field_profil,
        ]);
        $teachers = $DB->count_records_sql($sql, [
            'profil'    => 'teacher',
            'timestart' => $time, 
            'field_profil' => $this->user_info_field_profil,
        ]);
        
        return [
            'logins'                => $logins,
            'connections'           => $connections,
            'connections_students'  => $students,
            'connections_teachers'  => $teachers,
        ];
    }
    
    //Etablit la liste des derniers évenements (connexion, cours regardé) survenus sur la plate-forme.
    //Inutilisé par Skyeye pour l'instant.
    public function get_last_events($count = 10){
        global $DB;
        $sql =  'SELECT id,eventname,timecreated FROM {logstore_standard_log} '.
                'WHERE (action = "viewed" AND target = "course") '.
                'OR (action = "loggedin" AND target = "user") '.
                'ORDER BY id DESC LIMIT 10';
        $last_events = $DB->get_records_sql($sql,[]);
        return $last_events;
    }  

}
?>
