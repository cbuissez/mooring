<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Stats table model
 *
 * @package     local_mooring
 * @author      Charly Piva
 * @copyright   (C) Charly Piva 2017
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_mooring\local\models;

class stats_table {
    
    private $user_info_field_profil;
    private $user_info_field_uai;
    
    public function __construct($profil, $uai){
        $this->user_info_field_profil = $profil;
        $this->user_info_field_uai = $uai;
    }
    
    //Return arrays of day, week and month timestamps we should retrive logs on
    public function get_stats_times($time = 0, $iterations = 7) {
        if($time == 0) $time = time();
        
        //Latest day we'll retrieve stats on
        $day_start = new \DateTime();
        $day_start->setTimestamp($time);
        $day_start->modify('midnight');
        $day_start = $day_start->getTimestamp();
        
        //Latest week we'll retreive stats on
        $week_start = new \DateTime();
        $week_start->setTimestamp($time);
        $week_start->modify('midnight');
        $week_start->modify('previous monday');
        $week_start = $week_start->getTimestamp();
        
        //Latest month we'll retrieve stats on
        $month_start = new \DateTime();
        $month_start->setTimestamp($time);
        $month_start->modify('midnight');
        $month_start->modify('first day of this month');
        $month_start = $month_start->getTimestamp();

        //Preparing array of arrays
        $stats_times = [
            'daily'     => [$day_start], 
            'weekly'    => [$week_start], 
            'monthly'   => [$month_start]
        ];
        
        //Adding earlier days, weeks and months' timestamps
        for ($i = 1; $i < $iterations; $i++) {
            $day = new \DateTime();
            $day->setTimestamp($stats_times['daily'][$i - 1]);
            $day->modify('last day');
            $stats_times['daily'][$i] = $day->getTimestamp();
            $week = new \DateTime();
            $week->setTimestamp($stats_times['weekly'][$i - 1]);
            $week->modify('last week');
            $stats_times['weekly'][$i] = $week->getTimestamp();
            $month = new \DateTime();
            $month->setTimestamp($stats_times['monthly'][$i - 1]);
            $month->modify('last month');
            $stats_times['monthly'][$i] = $month->getTimestamp();    
        }

        return $stats_times;
    }
    
    //Renvoie le nombre de connexions et d'utilisateurs uniques pour les 7 derniers jours, semaines et mois
    public function get_logins($time = 0, $iterations = 7, $readableTimestamp = false, $type_etab = '') {
        global $DB;
        $logins = array();
        $stats_times = $this->get_stats_times($time, $iterations);

        foreach(array('daily','weekly','monthly') as $type) {
            $logins[$type] = [];
            for($i = 0; $i < $iterations; $i++) {
                $timeend = $stats_times[$type][$i];
                
                //On zappe ces stats si on cherche des stats journalières et qu'on remonte à plus de 3 mois
                //ou si on cherche des stats par semaine et qu'on remonte à plus de 9 mois
                $diff = time() - $timeend;
                if(($type == "daily" && $diff >= 60*60*24*30*3)
                || ($type == "weekly" && $diff >= 60*60*24*30*9)) {
                    continue;
                }

                //Si on cherche un type d'établissement précis
                if($type_etab){
                    
                    //Nombre de connexions
                    $sql =   'SELECT SUM(s.statsreads) as somme
                                FROM {stats_user_'.$type.'} s 
                          INNER JOIN {user_info_data} d 
                                  ON d.userid = s.userid
                                 AND d.fieldid = :field_uai
                          INNER JOIN {local_mooring_school} sc 
                                  ON sc.uai = d.data
                                 AND sc.nature = :type_etab
                               WHERE s.stattype = "logins" 
                                 AND s.timeend = :timeend';
                    $all = $DB->get_record_sql($sql, [
                            'type_etab'     => $type_etab,
                            'timeend'       => $timeend, 
                            'field_uai'  => $this->user_info_field_uai,
                    ])->somme;
                    if(!$all) $all = 0;
                    
                    //Nombre d'utilisateurs
                    $sql =   'SELECT COUNT(DISTINCT(s.userid)) 
                                FROM {stats_user_'.$type.'} s 
                          INNER JOIN {user_info_data} d 
                                  ON d.userid = s.userid
                                 AND d.fieldid = :field_uai
                          INNER JOIN {local_mooring_school} sc 
                                  ON sc.uai = d.data
                                 AND sc.nature = :type_etab
                               WHERE s.timeend = :timeend';
                    $unique = $DB->count_records_sql($sql, [
                            'type_etab'     => $type_etab,
                            'timeend'       => $timeend, 
                            'field_uai'  => $this->user_info_field_uai,
                    ]);
                    
                    //Nombre d'élèves et d'enseignants
                    $sql =   'SELECT COUNT(DISTINCT(s.userid)) 
                                FROM {stats_user_'.$type.'} s 
                          INNER JOIN {user_info_data} da 
                                  ON da.userid = s.userid
                                 AND da.fieldid = :field_uai
                          INNER JOIN {user_info_data} db 
                                  ON db.userid = s.userid
                                 AND db.fieldid = :field_profil 
                                 AND db.data = :profil
                          INNER JOIN {local_mooring_school} sc 
                                  ON sc.uai = da.data
                                 AND sc.nature = :type_etab
                               WHERE s.stattype = "logins" 
                                 AND s.timeend = :timeend';
                    $unique_students = $DB->count_records_sql($sql, [
                            'type_etab'     => $type_etab,
                            'timeend'       => $timeend, 
                            'field_uai'     => $this->user_info_field_uai,
                            'profil'        => 'student',
                            'field_profil'  => $this->user_info_field_profil,
                    ]);
                    $unique_teachers = $DB->count_records_sql($sql, [
                            'type_etab'     => $type_etab,
                            'timeend'       => $timeend, 
                            'field_uai'     => $this->user_info_field_uai,
                            'profil'        => 'teacher',
                            'field_profil'  => $this->user_info_field_profil,
                    ]);
                }    
                //Si on veut compter tous les utilisateurs, indépendemment d'un type d'établissement
                else{
                    //Nombre de connexions
                    $sql =   'SELECT SUM(s.statsreads) as somme
                                FROM {stats_user_'.$type.'} s 
                          INNER JOIN {user_info_data} d 
                                  ON d.userid = s.userid
                               WHERE s.stattype = "logins" 
                                 AND s.timeend = :timeend';
                    $all = $DB->get_record_sql($sql, [
                            'timeend'       => $timeend, 
                    ])->somme;
                    if(!$all) $all = 0;
                    
                    //Nombre d'utilisateurs
                    $sql =   'SELECT COUNT(DISTINCT(s.userid)) 
                                FROM {stats_user_'.$type.'} s 
                          INNER JOIN {user_info_data} d 
                                  ON d.userid = s.userid
                               WHERE s.timeend = :timeend';
                    $unique = $DB->count_records_sql($sql, [
                            'timeend'       => $timeend, 
                    ]);
                    
                    //Nombre d'élèves et d'enseignants
                    $sql = 'SELECT COUNT(*) 
                             FROM {stats_user_'.$type.'} s 
                       INNER JOIN {user_info_data} d 
                               ON d.userid = s.userid 
                              AND d.fieldid = :field_profil 
                              AND d.data = :profil 
                            WHERE s.stattype = "logins" 
                              AND s.timeend = :timeend';
                    $unique_students = $DB->count_records_sql($sql, [
                            'profil'    => 'student',
                            'timeend'   => $timeend, 
                            'field_profil' => $this->user_info_field_profil,
                    ]);
                    $unique_teachers = $DB->count_records_sql($sql, [
                                'profil'    => 'teacher',
                                'timeend'   => $timeend, 
                                'field_profil' => $this->user_info_field_profil,
                    ]);
                }

                //Si on n'a pas récupéré de stats, mettre 0 à la place car Moodle n'ajoute pas d'entrée dans la table en ce cas
                $preciseTimestamp = false;
                if(($type == 'daily' && $iterations > 7) || ($type == 'monthly' && $iterations > 12)) $preciseTimestamp = true;
                $logins[$type][] = [
                    'timestamp'         => $readableTimestamp ? $this->display_stats_timestamp($type, $timeend, $preciseTimestamp) : $timeend, 
                    'all'               => $all, 
                    'unique'            => $unique,
                    'unique_students'   => $unique_students,
                    'unique_teachers'   => $unique_teachers,
                ];
            }
            
        }
        return $logins;
    }
 
    //Renvoie le nombre de connexions d'élèves et d'enseignants dans un établissement donné
    public function get_login_array_by_school($uai, $time = 0, $iterations = 7, $readableTimestamp = false)
    {
        global $DB;
        $logins = array();
        $stats_times = $this->get_stats_times($time, $iterations);

        foreach (array('daily', 'weekly', 'monthly') as $type) {
            $logins[$type] = [];
            for ($i = 0; $i < $iterations; $i++) {
                $timeend = $stats_times[$type][$i];

                $login = $this->get_login_by_school($uai, $type, $timeend);
                $logins[$type][] = [
                    'timestamp' => $readableTimestamp ? $this->display_stats_timestamp($type, $timeend, true) : $timeend,
                    'unique_students' => $login['unique_students'],
                    'unique_teachers' => $login['unique_teachers'],
                ];
            }
        }
        return $logins;
    }
    
    private function get_login_by_school($uai, $type, $timeend){
        global $DB;
        
        $sql = 'SELECT COUNT(DISTINCT s.userid) 
                        FROM {stats_user_'.$type.'} s 
                  INNER JOIN {user_info_data} da ON da.userid = s.userid 
                  INNER JOIN {user_info_data} db ON db.userid = s.userid 
                       WHERE da.fieldid = :field_profil AND da.data = :profil
                         AND db.fieldid = :field_uai AND db.data = :uai
                         AND s.stattype = "logins"
                         AND timeend = :timeend';
        
        $logins_unique_students = $DB->count_records_sql($sql, [
            'profil'        => 'student',
            'uai'           => $uai,
            'timeend'       => $timeend, 
            'field_profil'  => $this->user_info_field_profil,
            'field_uai'     => $this->user_info_field_uai,
        ]);
        $logins_unique_teachers = $DB->count_records_sql($sql, [
            'profil'        => 'teacher',
            'uai'           => $uai,
            'timeend'       => $timeend, 
            'field_profil'  => $this->user_info_field_profil,
            'field_uai'     => $this->user_info_field_uai,
        ]);
        
        return [
            'unique_students'   => $logins_unique_students,
            'unique_teachers'   => $logins_unique_teachers
        ];
    }

    public function get_other_stats(){
        global $DB;
        $stats = [];
        
        //Nombre d'établissements
        $stats['schools'] = $DB->count_records_sql('SELECT COUNT(*) FROM {local_mooring_school}');
                
        //Nombre de cours créés depuis le début
        $stats['created_courses'] = (int) $DB->get_field_sql('SELECT (id) FROM {course} ORDER BY id DESC LIMIT 1');

        /*
         * Nombre d'étudiants et d'enseignants inscrits au moment où la statistique est relevée.
         * Jointure nécessaire pour savoir si un utilisateur donné est étudiant ou enseignant.
         */
        $sql = 'SELECT COUNT(*) FROM {user} u INNER JOIN {user_info_data} d ON d.userid = u.id '
               .'WHERE d.fieldid = :field_profil AND d.data = :data AND u.confirmed = 1 AND u.deleted = 0 AND u.suspended = 0';
        $stats['nb_students'] = $DB->count_records_sql($sql,[
                    'field_profil'  => $this->user_info_field_profil,
                    'data'          => 'student',
                ]);
        $stats['nb_teachers'] = $DB->count_records_sql($sql,[
                    'field_profil'  => $this->user_info_field_profil,
                    'data'          => 'teacher',
                ]);
        
        /* Compte tous les parcours dans lesquels au moins une cohorte a été injectée. */
        $stats['nb_courses_with_cohort'] = $DB->count_records_sql('SELECT COUNT(DISTINCT courseid) FROM {enrol} WHERE enrol = "cohort"');
        
        /*
         * Relève les enseignants créateurs et éditeurs
         * ayant créé un parcours dans lequel au moins une cohorte a été injectée.
         * Jointure sur context pour connaître le parcours correspondant au rôle
         * "coursecreator" ou "editingteacher",
         * puis jointure sur enrol pour savoir si une cohorte a été injectée
         * dans le parcours donné.
         * TODO : remplacer "2" et "3" par l'id des rôles "coursecreator" et "editingteacher"
         */
        $sql = 'SELECT ra.id as idra, u.id, u.firstname, u.lastname, e.courseid, d2.data, ms.name, ms.city FROM {user} u
                INNER JOIN {role_assignments} ra ON u.id = ra.userid
                INNER JOIN {context} c ON c.id = ra.contextid
                INNER JOIN {enrol} e ON e.courseid = c.instanceid
                INNER JOIN {user_info_data} d2 ON d2.userid = u.id
                LEFT JOIN {local_mooring_school} ms ON ms.uai = d2.data
                WHERE c.contextlevel = 50 AND ra.roleid <= :roleid AND e.enrol = "cohort" AND d2.fieldid = :field_uai';
        $stats['coursecreators_with_cohort'] = $this->group_teachers($DB->get_records_sql($sql,[
            'roleid'               => 2,
            'field_uai'            => $this->user_info_field_uai,
        ]));
        $stats['editingteachers_with_cohort'] = $this->group_teachers($DB->get_records_sql($sql,[
            'roleid'               => 3,
            'field_uai'            => $this->user_info_field_uai,
        ]));
        $stats['nb_coursecreators_with_cohort'] = count($stats['coursecreators_with_cohort']);
        $stats['nb_editingteachers_with_cohort'] = count($stats['editingteachers_with_cohort']);

        return $stats;
    }

    private function group_teachers($array){
        $ret = [];
        foreach($array as $teacher){
            unset($teacher->idra);
            if(isset($ret[$teacher->id])){
                if(!in_array($teacher->courseid,$ret[$teacher->id]->courses)) {
                    $ret[$teacher->id]->courses[] = $teacher->courseid;
                }
            } else {
                $ret[$teacher->id] = $teacher;
                $ret[$teacher->id]->courses = [$teacher->courseid];
            }
            unset($teacher->courseid);
        }
        foreach($ret as $id => $teacher){
            $ret[$id]->courses = count($ret[$id]->courses);
        }
        return $ret;
    }

    public function get_other_stats_by_school($uai){
        global $DB;
        $stats = [];

        /*
         * Nombre d'étudiants et d'enseignants inscrits au moment où la statistique est relevée.
         * Jointure nécessaire pour savoir si un utilisateur donné est étudiant ou enseignant.
         */
        $sql = 'SELECT COUNT(*) 
                  FROM {user} u   
            INNER JOIN {user_info_data} d1 
                    ON d1.userid = u.id 
            INNER JOIN {user_info_data} d2 
                    ON d2.userid = u.id 
                 WHERE d1.fieldid = :field_profil 
                   AND d1.data = :data_profil 
                   AND d2.fieldid = :field_uai
                   AND d2.data = :data_uai 
                   AND u.confirmed = 1 
                   AND u.deleted = 0 
                   AND u.suspended = 0';
        $stats['nb_students'] = $DB->count_records_sql($sql,[
            'field_profil'         => $this->user_info_field_profil,
            'data_profil'          => 'student',
            'field_uai'            => $this->user_info_field_uai,
            'data_uai'             => $uai
        ]);
        $stats['nb_teachers'] = $DB->count_records_sql($sql,[
            'field_profil'         => $this->user_info_field_profil,
            'data_profil'          => 'teacher',
            'field_uai'            => $this->user_info_field_uai,
            'data_uai'             => $uai
        ]);

        /* Compte tous les parcours dans lesquels au moins une cohorte a été injectée. */
        $sql = 'SELECT COUNT(DISTINCT courseid)
                  FROM {enrol} e
            INNER JOIN {course} c
                    ON e.courseid = c.id
            INNER JOIN {course_categories} cc
                    ON c.category = cc.id 
                 WHERE cc.name = :uai AND e.enrol = "cohort"';
        $stats['nb_courses_with_cohort'] = $DB->count_records_sql($sql,[
            'uai' => $uai,
        ]);

        /* Compter le nombre de profs créateurs de tels parcours
        TODO : remplacer "2" et "3" par l'id des rôles "coursecreator" et "editingteacher"
        */
        $sql = 'SELECT ra.id as idra, u.id, u.firstname, u.lastname, e.courseid FROM {user} u
                INNER JOIN {role_assignments} ra ON u.id = ra.userid
                INNER JOIN {context} c ON c.id = ra.contextid
                INNER JOIN {enrol} e ON e.courseid = c.instanceid
                INNER JOIN {user_info_data} d2 ON d2.userid = u.id
                WHERE c.contextlevel = 50 AND ra.roleid <= :roleid AND e.enrol = "cohort" AND d2.fieldid = :field_uai AND d2.data = :uai';
        $stats['coursecreators_with_cohort'] = $this->group_teachers($DB->get_records_sql($sql,[
            'roleid'               => 2,
            'field_uai'            => $this->user_info_field_uai,
            'uai'                  => $uai
        ]));
        $stats['editingteachers_with_cohort'] = $this->group_teachers($DB->get_records_sql($sql,[
            'roleid'               => 3,
            'field_uai'            => $this->user_info_field_uai,
            'uai'                  => $uai
        ]));
        $stats['nb_coursecreators_with_cohort'] = count($stats['coursecreators_with_cohort']);
        $stats['nb_editingteachers_with_cohort'] = count($stats['editingteachers_with_cohort']);

        return $stats;
    }
    
    private function display_stats_timestamp($type, $timestamp, $precis = false){
        $datetime = new \DateTime();
        $datetime->setTimestamp($timestamp);
        switch($type){
            case "monthly":
                $datetime->modify('-1 month');
                $idmonth = date('n', $datetime->getTimestamp()) - 1;
                $ret = ['janv.','févr.','mars','avr.','mai','juin','juil.','août','sept.','oct.','nov.','déc.'][$idmonth];
                $ret .= $precis ? ' '.date('y', $datetime->getTimestamp()) : '';
                return $ret;
            break;
            case "weekly":
                $datetime->modify('-1 week');
                return date('d/m',$datetime->getTimestamp());
            break;
            default:
                $datetime->modify('-1 day');
                $idday = date('w',$datetime->getTimestamp());
                $ret = ['dim','lun','mar','mer','jeu','ven','sam'][$idday].'.';
                $ret .= $precis ? ' '.date('d', $datetime->getTimestamp()) : '';
                return $ret;
            break;
        }
    }

}

?>
