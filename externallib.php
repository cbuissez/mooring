<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * External library
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @author      Charly Piva
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once($CFG->libdir . '/externallib.php');

use local_mooring\local\config;

class local_mooring_external extends external_api {
    
    /*public static function create_schools_parameters() {
        return new external_function_parameters(
            [
                'ids' => new external_multiple_structure(
                    new external_value(PARAM_NOTAGS, 'uai'),
                    'List of school ids',
                    VALUE_DEFAULT,
                    [],
                    NULL_ALLOWED
                )
            ]
        );
    }
    
    public static function create_schools_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                [
                    'uai' => new external_value(PARAM_NOTAGS, 'uai'),
                    'dept' => new external_value(PARAM_INT, 'dept'),
                    'cas' => new external_value(PARAM_NOTAGS, 'cas'),
                    'time' => new external_value(PARAM_INT, 'time'),
                    'who' => new external_value(PARAM_NOTAGS, 'who')
                ]
            )
        );
    }
    
    public static function create_schools($ids) {
        global $DB;
        
        $params = self::validate_parameters(self::create_schools_parameters(), array('ids' => $ids));
        $transaction = $DB->start_delegated_transaction();
 
        $obj = new local_platformagent\local\models\school_table();
        
        $sschools = [];
        foreach ($params['ids'] as $uai) {
            $context = get_context_instance(CONTEXT_SYSTEM);
            self::validate_context($context);
            //require_capability('moodle/course:managegroups', $context);
            
            $sschools[] = (array) $obj->create((object) ['uai' => $uai]);
        }
 
        $transaction->allow_commit();
        return $sschools;
    }*/
    public static function get_param_list_parameters() {
        return new external_function_parameters([]);
    }

    public static function get_param_list_returns() {
        return new external_multiple_structure(
            new external_single_structure([
                'plugin' => new external_value(PARAM_NOTAGS, 'plugin'),
                'params' => new external_multiple_structure(
                    new external_single_structure([
                        'param'     => new external_value(PARAM_NOTAGS, 'param'),
                        'editable'  => new external_value(PARAM_BOOL, 'editable'),
                        ])
                )
            ])
        );
    }

    public static function get_param_list() {
        global $DB;
        $list = ['CORE' => [
            'plugin' => 'CORE',
            'params' => []
            ]];
        //Core params
        foreach($DB->get_records('config') as $conf) {
            $list['CORE']['params'][] = [
                'param' => $conf->name,
                'editable' => config::load('editableparams')->is_editable_param('CORE', $conf->name)
            ];
        }
        //Plugin params
        foreach($DB->get_records('config_plugins') as $conf){
            if($conf->name != 'version') {
                if (!isset($list[$conf->plugin])) {
                    $list[$conf->plugin] = [
                        'plugin' => $conf->plugin,
                        'params' => []
                    ];
                }
                $list[$conf->plugin]['params'][] = [
                    'param' => $conf->name,
                    'editable' => config::load('editableparams')->is_editable_param($conf->plugin, $conf->name)
                ];
            }
        }
        return $list;
    }



    public static function get_plugin_param_parameters() {
        return new external_function_parameters(
            [
                'plugin'          => new external_value(PARAM_TEXT, 'plugin'),
                'param'          => new external_value(PARAM_TEXT, 'param'),
            ]
        );
    }

    public static function get_plugin_param_returns() {
        return new external_value(PARAM_RAW, 'value');
    }

    public static function get_plugin_param($plugin, $param) {
        global $CFG;
        if($plugin == 'CORE'){
            return $CFG->$param;
        }
        return get_config($plugin, $param);
    }



    public static function set_plugin_param_parameters() {
        return new external_function_parameters(
            [
                'plugin'         => new external_value(PARAM_TEXT, 'plugin'),
                'param'          => new external_value(PARAM_TEXT, 'param'),
                'value'          => new external_value(PARAM_RAW_TRIMMED, 'value'),
            ]
        );
    }

    public static function set_plugin_param_returns() {
        return new external_value(PARAM_BOOL, 'result');
    }

    public static function set_plugin_param($plugin, $param, $new_value) {
        global $CFG;
        if(config::load('editableparams')->is_editable_param($plugin,$param)) {
            if($plugin == 'CORE' && $CFG->$param !== null) {
                set_config($param, $new_value);
                return true;
            } else if ($plugin && get_config($plugin, $param) !== false) {
                set_config($param, $new_value, $plugin);
                return true;
            }
        }
        return false;
    }

    public static function get_schools_parameters() {
        return new external_function_parameters(
            [
                // NO PARAMETER NEEDED
            ]
        );
    }
    
    public static function get_schools_returns() {
        return new external_multiple_structure(
            new external_single_structure([
                'name'              => new external_value(PARAM_NOTAGS, 'name'),
                'city'              => new external_value(PARAM_NOTAGS, 'city'),
                'uai'               => new external_value(PARAM_NOTAGS, 'uai'),
                'cas'               => new external_value(PARAM_NOTAGS, 'casname'),
                'importclass'       => new external_value(PARAM_NOTAGS, 'importclass'),
                'importago'         => new external_value(PARAM_NOTAGS, 'importago'),
                'importtimestamp'   => new external_value(PARAM_NOTAGS, 'importtimestamp')
            ])
        );
    }
    
    public static function get_schools() {
        $obj = new local_platformagent\local\models\school_table();
        $schoolsobj = $obj->all();
        $schools = [];
        foreach ($schoolsobj as $schoolobj) {
            $school = (array) $schoolobj;
            $school['cas'] = $schoolobj->casname;
            $school['importclass'] = $schoolobj->studentslastimport->class;
            $school['importago'] = $schoolobj->studentslastimport->timeago;
            $school['importtimestamp'] = $schoolobj->studentslastimport->timestamp;
            $schools[] = $school;
        }
        return $schools;
    }
    
    public static function stats_parameters() {
        return new external_function_parameters([
                'timestamp_start' => new external_value(PARAM_INT, 'Timestamp from when we should retrieve stats')
            ]
        );
    }
    
    public static function stats_returns() {
        $return = [];
        
        $logins_structure_periods = [];
        foreach(['daily', 'weekly', 'monthly'] as $type) {
            $logins_structure = new external_single_structure([
                                            'timestamp'             => new external_value(PARAM_NOTAGS, ''), 
                                            'all'                   => new external_value(PARAM_NOTAGS, ''),
                                            'unique'                => new external_value(PARAM_NOTAGS, ''),
                                            'unique_students'       => new external_value(PARAM_NOTAGS, ''),
                                            'unique_teachers'       => new external_value(PARAM_NOTAGS, ''),
                                ]);
            $return[$type] = new external_multiple_structure($logins_structure);
            $logins_structure_periods[$type] = new external_multiple_structure($logins_structure);
        }
        
        foreach(['ecole','college','lycee'] as $type_etab){
            $return[$type_etab] = new external_single_structure($logins_structure_periods);
        }
        
        foreach([
            'created_courses',
            'nb_students',
            'nb_teachers',
            'nb_courses_with_cohort',
            'nb_coursecreators_with_cohort',
            'nb_editingteachers_with_cohort',
            'schools',
        ] as $type){
            $return[$type] = new external_value(PARAM_INT, '');
        }
        foreach([
            'coursecreators_with_cohort',
            'editingteachers_with_cohort',
        ] as $type){
            $return[$type] = new external_multiple_structure(
                new external_single_structure([
                    'id'             => new external_value(PARAM_NOTAGS, ''), 
                    'firstname'      => new external_value(PARAM_NOTAGS, ''),
                    'lastname'       => new external_value(PARAM_NOTAGS, ''),
                    'courses'        => new external_value(PARAM_INT, ''),
                    'data'           => new external_value(PARAM_NOTAGS, ''),
                    'name'           => new external_value(PARAM_NOTAGS, ''),
                    'city'           => new external_value(PARAM_NOTAGS, ''),
                ])
            );
        }
        return new external_single_structure($return);
    }
    
    public static function stats($timestamp_start) {
        $profil = config::load()->get_user_field_id('profil');
        $uai = config::load()->get_user_field_id('uai');
        $obj = new local_mooring\local\models\stats_table($profil,$uai);
        
        $logins = $obj->get_logins($timestamp_start,7,false,'');
        $logins_by_type = [];
        foreach(['ecole','college','lycee'] as $type_etab){
            $logins_by_type[$type_etab] = $obj->get_logins($timestamp_start,7,false,$type_etab);
        }
        
        $other = $obj->get_other_stats($timestamp_start);
        
        return array_merge($logins,$logins_by_type,$other);
    }
    
    public static function lastlogs_parameters(){
        return new external_function_parameters([
                'timestamp_start' => new external_value(PARAM_INT, 'Timestamp from when we should retrieve stats')
            ]
        ); 
    }
    
    public static function lastlogs_returns() {
        return new external_single_structure([
            'counts'        => new external_multiple_structure(
                                new external_value(PARAM_INT)
                            ),
            'counts_start'  => new external_value(PARAM_INT),
            'currentday'    => new external_single_structure([
                                'logins'             => new external_value(PARAM_INT), 
                                'connections'             => new external_value(PARAM_INT), 
                                'connections_students'    => new external_value(PARAM_INT), 
                                'connections_teachers'    => new external_value(PARAM_INT), 
                            ]),
            'last'          => new external_multiple_structure(
                                new external_single_structure([
                                    'id'            => new external_value(PARAM_NOTAGS, ''), 
                                    'eventname'     => new external_value(PARAM_NOTAGS, ''),
                                    'timecreated'   => new external_value(PARAM_NOTAGS, ''),
                                ]) 
                        ),
        ]);
    }
 
    public static function lastlogs($timestamp_start){
        $profil = config::load()->get_user_field_id('profil');
        $uai = config::load()->get_user_field_id('uai');
        $obj = new local_mooring\local\models\logs_table($profil,$uai);
        $counts = $obj->get_user_counts($timestamp_start);
        return [
            'counts'        => $counts->values,
            'counts_start'  => $counts->timestamp_start,
            'currentday'    => $obj->get_current_day_connections($timestamp_start),
            'last'          => $obj->get_last_events()
        ];
    }   
}