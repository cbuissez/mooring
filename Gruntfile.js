// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Grunt script
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   2014 Andrew Nicols
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

module.exports = function(grunt) {
    
    var path = require('path'),
        cwd = process.env.PWD || process.cwd()

    // PHP strings for exec task.
    var moodleroot = path.dirname(path.dirname(__dirname)),
        configfile = '',
        decachephp = '',
        dirrootopt = grunt.option('dirroot') || process.env.MOODLE_DIR || '';

    // Allow user to explicitly define Moodle root dir.
    if ('' !== dirrootopt) {
        moodleroot = path.resolve(dirrootopt);
    }
    
    configfile = path.join(moodleroot, 'config.php');

    decachephp += 'define(\'CLI_SCRIPT\', true);';
    decachephp += 'require(\'' + configfile + '\');';
    decachephp += 'js_reset_all_caches();';

    /**
     * Function to generate the destination for the uglify task
     * (e.g. build/file.min.js). This function will be passed to
     * the rename property of files array when building dynamically:
     * http://gruntjs.com/configuring-tasks#building-the-files-object-dynamically
     *
     * @param {String} destPath the current destination
     * @param {String} srcPath the  matched src path
     * @return {String} The rewritten destination path.
     */
    var uglifyRename = function(destPath, srcPath) {
        destPath = srcPath.replace('src', 'build');
        destPath = destPath.replace('.js', '.min.js');
        destPath = path.resolve(cwd, destPath);
        return destPath;
    };

    // Project configuration.
    grunt.initConfig({
        uglify: {
            mooring: {
                files: [{
                    expand: true,
                    src: '**/amd/src/*.js',
                    rename: uglifyRename
                }],
                options: {report: 'none'}
            },
            platformagent: {
                files: [{
                    expand: true,
                    src: '../platformagent/**/amd/src/*.js',
                    rename: uglifyRename
                }],
                options: {report: 'none'}
            },
            massimilate: {
                files: [{
                    expand: true,
                    src: '../massimilate/**/amd/src/*.js',
                    rename: uglifyRename
                }],
                options: {report: 'none'}
            }
        },
        exec: {
            decache: {
                cmd: 'php -r "' + decachephp + '"',
                callback: function(error) {
                    // exec will output error messages
                    // just add one to confirm success.
                    if (!error) {
                        grunt.log.writeln("Moodle js cache reset.");
                    }
                }
            }
        },
        watch: {
            options: {
                nospawn: true // We need not to spawn so config can be changed dynamically.
            },
            mooring: {
                files: ['**/amd/src/**/*.js'],
                tasks: ['uglify:watch', 'decache']
            },
            platformagent: {
                files: ['../platformagent/**/amd/src/**/*.js'],
                tasks: ['uglify:watch', 'decache']
            },
            massimilate: {
                files: ['../massimilate/**/amd/src/**/*.js'],
                tasks: ['uglify:watch', 'decache']
            }
        }
    });

    // On watch, we dynamically modify config to build only affected files. This
    // method is slightly complicated to deal with multiple changed files at once (copied
    // from the grunt-contrib-watch readme).
    var changedFiles = Object.create(null);
    var onChange = grunt.util._.debounce(function() {
          var files = Object.keys(changedFiles);
          grunt.config('uglify.watch.files', [{expand: true, src: files, rename: uglifyRename}]);
          changedFiles = Object.create(null);
    }, 200);

    grunt.event.on('watch', function(action, filepath) {
          changedFiles[filepath] = action;
          onChange();
    });

    // Register NPM tasks.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-exec');
    
    // Register tasks.
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('decache', ['exec:decache']);
    grunt.registerTask('install', ['uglify']);

};
