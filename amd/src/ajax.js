// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Ajax library
 *
 * @package     mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['local_mooring/URI'], function(URI) {
    
    var get = function(url, data) {
        if (typeof data !== 'undefined')
            url = URI(url).normalizeQuery().addQuery(data).toString()
        return request('GET', url, null)
    }

    var post = function(url, data) {
        url = URI(url).normalizeQuery().toString()
        var formData = toFormData(data)
        return request('POST', url, formData)
    }
    
    var handle = function(response) {
        var json = JSON.parse(response)
        if (typeof json.error !== 'undefined')
            throw new Error(json.error)
        return json.data
    }

    var request = function(type, url, formData) {
        return new Promise(function(resolve, reject) {
            var request = new XMLHttpRequest()
            request.open(type, url)
            request.onload = function() {
                if (request.status === 200)
                    resolve(request.responseText)
                else
                    reject(Error(request.statusText))
            }
            request.onerror = function() {
                reject(Error("AJAX: connection error"))
            }
            request.send(formData)
        })
    }
    
    var toFormData = function(data) {
        var formData = new FormData()
        var authorizeTypes = ['string', 'number', 'boolean']
        for (var index in data) {
            if (typeof data[index] === 'object') {
                for (var i in data[index]) {
                    if (authorizeTypes.indexOf(typeof data[index][i]) !== -1)
                        formData.append(index + '[]', data[index][i])
                }
            }
            else if (authorizeTypes.indexOf(typeof data[index]) !== -1)
                formData.append(index, data[index])
        }
        return formData
    }
    
    return {
        get: get,
        post: post,
        handle: handle
    }
    
})